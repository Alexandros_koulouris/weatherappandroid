﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Locations;
using com.Weather.App.Model;
using Android.Support.V4.Widget;
using Newtonsoft.Json;
using Square.Picasso;
using Xyz.Matteobattilana.Library;
using static Xyz.Matteobattilana.Library.Common.Constants;
using System.Threading.Tasks;
using static Android.Support.V4.Widget.SwipeRefreshLayout;

namespace com.Weather.App.Fragments
{
    public class WeatherToday : Android.Support.V4.App.Fragment , ILocationListener, IOnRefreshListener
    {


        TextView txtCity, txtLastUpdate, txtDescription, txtHumidity, txtTime, txtCelsius;
        ImageView imageView;
        LocationManager locationManager;
        string provider;
        static double lat, lng;
        WeatherMap weatherMap = new WeatherMap();
        SwipeRefreshLayout swipeContainer;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.Main, container, false);
            swipeContainer = view.FindViewById<SwipeRefreshLayout>(Resource.Id.slSwipeContainer);
           swipeContainer.SetOnRefreshListener(this);
            swipeContainer.SetColorSchemeResources(Android.Resource.Color.HoloBlueLight, Android.Resource.Color.HoloGreenLight, Android.Resource.Color.HoloOrangeLight, Android.Resource.Color.HoloRedLight);

            var weatherView = view.FindViewById<WeatherView>(Resource.Id.weather);
            weatherView.SetWeather(WeatherStatus.Rain)
                .SetLifeTime(2000)
                .SetFadeOutTime(1000)
                .SetParticles(43)
                .SetFPS(60)
                .SetAngle(-5)
                .StartAnimation();



            //locationManager = (LocationManager)GetSystemService(Context.LocationService);
            //provider = locationManager.GetBestProvider(new Criteria(), false);
            //Location location = locationManager.GetLastKnownLocation(LocationManager.PassiveProvider);
            //if (location == null)
            //{
            //    System.Diagnostics.Debug.WriteLine("No Location");
            //}
            

            //lat = location.Latitude;
            //lng = location.Longitude;


            lat = 40.730610;
            lng = -73.935242;
            new GetWeather(this, weatherMap, view).Execute(Common.Common.APIRequest(lat.ToString(), lng.ToString()));
            return view;

         

        }

        public async void OnRefresh()
        {
            await Task.Delay(2000);
            swipeContainer.Refreshing = false;
            new GetWeather(this, weatherMap,View).Execute(Common.Common.APIRequest(lat.ToString(), lng.ToString()));
        }


        public void OnLocationChanged(Location location)
        {
            throw new NotImplementedException();
        }

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }

        
        private class GetWeather : AsyncTask<string, Java.Lang.Void, string>
        {
          private ProgressDialog pd = new ProgressDialog(Application.Context);
          private WeatherToday fragmnet;
          WeatherMap weatherMap;
            View view;

            protected override string RunInBackground(params string[] @params)
            {
                string stream = null;
                string urlString = @params[0];
                Helper.Helper http = new Helper.Helper();
                urlString = Common.Common.APIRequest(lat.ToString(), lng.ToString());
                stream = http.GetHTTPData(urlString);
                return stream;
            }

            public GetWeather(WeatherToday fragmnet, WeatherMap weatherMap, View view)
            {
                this.fragmnet = fragmnet;
                this.weatherMap = weatherMap;
                this.view = view;
            }

            protected override void OnPreExecute()
            {
                base.OnPreExecute();
                pd.Window.SetType(Android.Views.WindowManagerTypes.SystemAlert);
                pd.SetTitle("please wait...");
                pd.Show();
            }

            protected override void OnPostExecute(string result)
            {
                base.OnPostExecute(result);
                if (result.Contains("Error: not found city"))
                {
                    pd.Dismiss();
                    return;
                }
                weatherMap = JsonConvert.DeserializeObject<WeatherMap>(result);
                pd.Dismiss();

                //controls
                fragmnet.txtCity = view.FindViewById<TextView>(Resource.Id.txtCity);
                fragmnet.txtLastUpdate = view.FindViewById<TextView>(Resource.Id.txtLastUpdate);
                fragmnet.txtDescription = view.FindViewById<TextView>(Resource.Id.txtDescription);
                fragmnet.txtHumidity = view.FindViewById<TextView>(Resource.Id.txtHumidity);
                fragmnet.txtTime = view.FindViewById<TextView>(Resource.Id.txtTime);
                fragmnet.txtCelsius = view.FindViewById<TextView>(Resource.Id.txtCelsius);
                fragmnet.imageView = view.FindViewById<ImageView>(Resource.Id.imageView);

                //add data
                fragmnet.txtCity.Text = $"{weatherMap.name},{weatherMap.sys.country}";
                fragmnet.txtLastUpdate.Text = $"Last updated:{DateTime.Now.ToString("dd MMMM yyyy HH:mm")}";
                fragmnet.txtDescription.Text = $"{weatherMap.weather[0].description}";
                fragmnet.txtHumidity.Text = $"Humiity:{weatherMap.main.humidity} %";
                fragmnet.txtTime.Text = $"{Common.Common.UnixTimeStampToDateTime(weatherMap.sys.sunrise).ToString()}\n{Common.Common.UnixTimeStampToDateTime(weatherMap.sys.sunset).ToString("HH:mm")}";
                fragmnet.txtCelsius.Text = $"{weatherMap.main.temp}°C";

                if (!string.IsNullOrEmpty(weatherMap.weather[0].icon))
                {
                    Picasso.With(view.Context)
                        .Load(Common.Common.GetImage(weatherMap.weather[0].icon))
                        .Into(fragmnet.imageView);
                }
                //Set colors for swipe container and attach a refresh listener


            }

        }

    }
}
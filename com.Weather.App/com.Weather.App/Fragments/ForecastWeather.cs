﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using com.Weather.App.Model;
using System.Threading.Tasks;
using Java.Lang;
using static Android.Support.V7.Widget.RecyclerView;

namespace com.Weather.App.Fragments
{
    public class ForecastWeather : Android.Support.V4.App.Fragment
    {

        private ListView ForeListView;
        static double lat, lng;
        WeatherMap weatherForecast = new WeatherMap();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.ForecastWeather, container, false);

            lat = 40.730610;
            lng = -73.935242;

            new GetWeather(this, weatherForecast, view).Execute(Common.Common.APIRequestForecast(lat.ToString(), lng.ToString()));

            // System.Threading
            //weatherForecast = JsonConvert.DeserializeObject<WeatherMap>(streamFore);

            //string temp = $"{weatherForecast.li}"


            //mItems = new List<string>();
            //mItems.Add("One");
            //mItems.Add("Two");
            //mItems.Add("Three");
            //mItems.Add("Four");
            //mItems.Add("Five");

            //ArrayAdapter<string> adapter = new ArrayAdapter<string>(Context, Android.Resource.Layout.SimpleListItem1, objects: mItems.ToArray());

            //ForeListView.Adapter = adapter;


            return view;

        }

        private class GetWeather : AsyncTask<string, Java.Lang.Void, string>
        {
            private ProgressDialog pd = new ProgressDialog(Application.Context);
            private ForecastWeather fragmnet;
            WeatherMap weatherMap;
            View view;

            protected override string RunInBackground(params string[] @params)
            {
                string stream = null;
                string urlString = @params[0];
                Helper.Helper http = new Helper.Helper();
                urlString = Common.Common.APIRequestForecast(lat.ToString(), lng.ToString());
                stream = http.GetHTTPData(urlString);
                return stream;
            }

            public GetWeather(ForecastWeather fragmnet, WeatherMap weatherMap, View view)
            {
                this.fragmnet = fragmnet;
                this.weatherMap = weatherMap;
                this.view = view;
            }

            protected override void OnPreExecute()
            {
                base.OnPreExecute();

            }

            protected override void OnPostExecute(string result)
            {

                base.OnPostExecute(result);
                if (result.Contains("Error: not found city"))
                {

                    return;
                }
                weatherMap = JsonConvert.DeserializeObject<WeatherMap>(result);
                ExpandableListView  ForeListView = view.FindViewById<ExpandableListView>(Resource.Id.ForecastListView);

                string[] values = new[] { $"{weatherMap.list[0].main.temp}", "iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2" };

                string[] children = new[] {"iPhone", "WindowsMobile",
                "Blackberry", "WebOS", "Ubuntu", "Windows7", "Max OS X",
                "Linux", "OS/2"};
                ForeListView.SetAdapter(new ExpandableListAdapter(view,values, children));
                ForeListView.SetGroupIndicator(null);

                //mItems = new List<string>();
                //mItems.Add("One");
                //mItems.Add("Two");
                //mItems.Add("Three");
                //mItems.Add("Four");
                //mItems.Add("Five");

                //ArrayAdapter<string> adapter = new ArrayAdapter<string>(view.Context, Android.Resource.Layout.SimpleExpandableListItem1, values);

                //fragmnet.ForeListView.Adapter = adapter;



            }

        }

        public class ExpandableListAdapter : BaseExpandableListAdapter
        {
            
            private System.String[] values;
            private System.String[] children;
            View viewlist;

            public ExpandableListAdapter(View view ,System.String[] values, System.String[] children)
            {
                this.values = values;
                this.children = children;
                this.viewlist = view;
                //inf = LayoutInflater.from(getActivity());
            }
            public override int GroupCount {
                get
                {
                    return values.Length;
                }
            }

            public override bool HasStableIds
            {
                get
                {
                    return true;
                }
            }
            public override Java.Lang.Object GetChild(int groupPosition, int childPosition)
            {
                return null;
            }

            public override long GetChildId(int groupPosition, int childPosition)
            {
                return childPosition;
            }

            public override int GetChildrenCount(int groupPosition)
            {
                return children[groupPosition].Length;
            }

            public override View GetChildView(int groupPosition, int childPosition, bool isLastChild, View convertView, ViewGroup parent)
            {
                var view = convertView;

                // If no recycled view, inflate a new view as a simple expandable list item 2:
                if (view == null)
                {
                    var inflater = viewlist.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
                    view = inflater.Inflate(Android.Resource.Layout.SimpleExpandableListItem2, null);
                }

                TextView textView = view.FindViewById<TextView>(Android.Resource.Id.Text1);
                textView.Text ="alex";
                return view;
            }

            public override Java.Lang.Object GetGroup(int groupPosition)
            {
                throw new NotImplementedException();
            }

            public override long GetGroupId(int groupPosition)
            {
                return groupPosition;
            }

            public override View GetGroupView(int groupPosition, bool isExpanded, View convertView, ViewGroup parent)
            {
                var view = convertView;

                if (view == null)
                {
                    var inflater = viewlist.Context.GetSystemService(Context.LayoutInflaterService) as LayoutInflater;
                    view = inflater.Inflate(Resource.Layout.list_item, null);
                }

                //setup groupview
                var produce = values[groupPosition];
                TextView textView = view.FindViewById<TextView>(Resource.Id.Text1);
                textView.Text = produce;
                TextView textView2 = view.FindViewById<TextView>(Resource.Id.Text2);
                DateTime x = DateTime.Today.AddDays(groupPosition+1);
                string day = x.DayOfWeek.ToString();
                textView2.Text = day;

                return view;
            }

            public override bool IsChildSelectable(int groupPosition, int childPosition)
            {
                return true;
            }
        }
    }
}
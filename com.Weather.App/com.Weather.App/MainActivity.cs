﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Locations;
using com.Weather.App.Model;
using Android.Runtime;
using System;
using Java.Lang;
using Newtonsoft.Json;
using Square.Picasso;
using Android.Content;
using Android.Support.V7.App;
using Xyz.Matteobattilana.Library;
using Android.Support.V4.Widget;
using System.Threading.Tasks;
using static Xyz.Matteobattilana.Library.Common.Constants;

namespace com.Weather.App
{
   
    [Activity(Label = "com.Weather.App", Theme = "@style/Theme.AppCompat.Light.DarkActionBar")]
    public class MainActivity : AppCompatActivity, ILocationListener
        


    {
        TextView txtCity, txtLastUpdate, txtDescription, txtHumidity, txtTime, txtCelsius;
        ImageView imageView;
        LocationManager locationManager;
        string provider;
        static double lat,lng;
        WeatherMap weatherMap = new WeatherMap();
        SwipeRefreshLayout swipeContainer;
        protected override void OnCreate(Bundle savedInstanceState)
        {

            
            base.OnCreate(savedInstanceState);
           
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

          

            locationManager = (LocationManager)GetSystemService(Context.LocationService);
            provider = locationManager.GetBestProvider(new Criteria(),false);
            Location location = locationManager.GetLastKnownLocation(LocationManager.PassiveProvider);
            if(location==null)
            {
                System.Diagnostics.Debug.WriteLine("No Location");
            }
            txtCity = FindViewById<TextView>(Resource.Id.txtCity);

            lat = location.Latitude;
            lng = location.Longitude;
            new GetWeather(this, weatherMap).Execute(Common.Common.APIRequest(lat.ToString(), lng.ToString()));

            Button weatherForeCast = FindViewById<Button>(Resource.Id.WeatherForeCast);
            weatherForeCast.Click += (sender, e) =>
            {
                var intent = new Intent(this, typeof(WeatherForeCastActivity));
                //intent.PutStringArrayListExtra("phone_numbers", phoneNumbers);
                StartActivity(intent);
            };
            
        }

       


        // React on swipe to refresh
        private async void SwipeContainer_Refresh(object sender, EventArgs e)
        {
            locationManager = (LocationManager)GetSystemService(Context.LocationService);
            provider = locationManager.GetBestProvider(new Criteria(), false);

            Location location = locationManager.GetLastKnownLocation(LocationManager.PassiveProvider);
            lat = location.Latitude;
            lng = location.Longitude;
            new GetWeather(this, weatherMap).Execute(Common.Common.APIRequest(lat.ToString(), lng.ToString()));
            await Task.Delay(2000);
            (sender as SwipeRefreshLayout).Refreshing = false;
        }

        public void OnLocationChanged(Location location)
        {
            //lat = System.Math.Round(location.Latitude, 4);
            //lng = System.Math.Round(location.Longitude, 4);
            //new GetWeather(this, weatherMap).Execute(Common.Common.APIRequest(lat.ToString(), lng.ToString()));

        }

        protected override void OnResume()
        {
            base.OnResume();
            locationManager.RequestLocationUpdates(provider,400,1,this);
        }

        protected override void OnPause()
        {
            base.OnPause();
            locationManager.RemoveUpdates(this);
        }

        public void OnProviderDisabled(string provider)
        {
            
        }

        public void OnProviderEnabled(string provider)
        {
            
        }

        public void OnStatusChanged(string provider, [GeneratedEnum] Availability status, Bundle extras)
        {
           
        }

        private class GetWeather : AsyncTask<string, Java.Lang.Void, string>
        {
            private ProgressDialog pd = new ProgressDialog(Application.Context);
            private MainActivity activity;
            WeatherMap weatherMap;

            public GetWeather(MainActivity activity, WeatherMap weatherMap)
            {
                this.activity = activity;
                this.weatherMap = weatherMap;
            }

            protected override void OnPreExecute()
            {
                base.OnPreExecute();
                pd.Window.SetType(Android.Views.WindowManagerTypes.SystemAlert);
                pd.SetTitle("please wait...");
                pd.Show();
            }
            protected override string RunInBackground(params string[] @params)
            {
                string stream = null;
                string urlString = @params[0];
                Helper.Helper http = new Helper.Helper();
                urlString = Common.Common.APIRequest(lat.ToString(), lng.ToString());
                stream = http.GetHTTPData(urlString);
                return stream;
            }
            protected override void OnPostExecute(string result)
            {
                base.OnPostExecute(result);
                if (result.Contains("Error: not found city"))
                {
                    pd.Dismiss();
                    return;
                }
                  weatherMap = JsonConvert.DeserializeObject<WeatherMap>(result);
                pd.Dismiss();
                //controls
                activity.txtCity = activity.FindViewById<TextView>(Resource.Id.txtCity);
                activity.txtLastUpdate = activity.FindViewById<TextView>(Resource.Id.txtLastUpdate);
                activity.txtDescription = activity.FindViewById<TextView>(Resource.Id.txtDescription);
                activity.txtHumidity = activity.FindViewById<TextView>(Resource.Id.txtHumidity);
                activity.txtTime = activity.FindViewById<TextView>(Resource.Id.txtTime);
                activity.txtCelsius = activity.FindViewById<TextView>(Resource.Id.txtCelsius);
                activity.imageView = activity.FindViewById<ImageView>(Resource.Id.imageView);

                //add data
                activity.txtCity.Text = $"{weatherMap.name},{weatherMap.sys.country}";
                activity.txtLastUpdate.Text = $"Last updated:{DateTime.Now.ToString("dd MMMM yyyy HH:mm")}";
                activity.txtDescription.Text = $"{weatherMap.weather[0].description}";
                activity.txtHumidity.Text = $"Humiity:{weatherMap.main.humidity} %";
                activity.txtTime.Text = $"{Common.Common.UnixTimeStampToDateTime(weatherMap.sys.sunrise).ToString()}\n{Common.Common.UnixTimeStampToDateTime(weatherMap.sys.sunset).ToString("HH:mm")}";
                activity.txtCelsius.Text = $"{weatherMap.main.temp}°C";

                if (!string.IsNullOrEmpty(weatherMap.weather[0].icon))
                {
                    Picasso.With(activity.ApplicationContext)
                        .Load(Common.Common.GetImage(weatherMap.weather[0].icon))
                        .Into(activity.imageView);
                }

                var weatherView = activity.FindViewById<WeatherView>(Resource.Id.weather);
                weatherView.SetWeather(WeatherStatus.Rain)
                    .SetLifeTime(2000)
                    .SetFadeOutTime(1000)
                    .SetParticles(43)
                    .SetFPS(60)
                    .SetAngle(-5)
                    .StartAnimation();

                //Set colors for swipe container and attach a refresh listener

               activity.swipeContainer = activity.FindViewById<SwipeRefreshLayout>(Resource.Id.slSwipeContainer);
                activity.swipeContainer.SetColorSchemeResources(Android.Resource.Color.HoloBlueLight, Android.Resource.Color.HoloGreenLight, Android.Resource.Color.HoloOrangeLight, Android.Resource.Color.HoloRedLight);
                activity.swipeContainer.Refresh += activity.SwipeContainer_Refresh;

            }
        }

    }
}


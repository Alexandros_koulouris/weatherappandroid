﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using static Android.Widget.TabHost;
using Android.Support.V4.View;
using Android.Support.Design.Widget;

namespace com.Weather.App
{
    [Activity(Label = "mpafj", MainLauncher = true, Icon = "@drawable/ic_tab_my_schedule_selected", Theme = "@style/Theme.AppCompat.Light.NoActionBar")]
    public class MenuTabActivity : AppCompatActivity 
        //, GestureDetector.IOnGestureListener, IOnTabChangeListener
    {
        
    //    GestureDetector gestureDetector;
    //    const int SWIPE_DISTANCE_THRESHOLD = 50;
    //    const int SWIPE_VELOCITY_THRESHOLD = 50;


    //    public bool OnDown(MotionEvent e)
    //    {
    //        return true;
    //    }

    //    public bool OnFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
    //    {
    //        float distanceX = e2.GetX() - e1.GetX();
    //        float distanceY = e2.GetY() - e1.GetY();
    //        if (Math.Abs(distanceX) > Math.Abs(distanceY) && Math.Abs(distanceX) > SWIPE_DISTANCE_THRESHOLD && Math.Abs(velocityX) > SWIPE_VELOCITY_THRESHOLD)
    //        {
    //            if (distanceX > 0)
    //                OnSwipeRight();
    //            else
    //                OnSwipeLeft();
    //            return true;
    //        }
    //        return false;
    //    }

    //    void OnSwipeLeft()
    //    {
    //        TabWidget.SetCurrentTab(1);
    //        TabHost.SetCurrentTabByTag("whats_l");
    //       // ImageView.Animate().Alpha(0.0f).XBy((float)WindowManager.DefaultDisplay.Width).SetDuration(2000).Start();

                

    ////        Animate()
    ////.Alpha(0.0f)
    ////.XBy((float)WindowManager.DefaultDisplay.Width)
    ////.SetDuration(2000)
    ////.Start();

    //        // var lintent = new Intent(this, typeof(MenuTabActivity));
    //        // StartActivity(lintent);

    //        //setCurrentTab(1
    //        //TabWidget.GetChildAt(1).Selected.ToString();
    //        //TabWidget.GetChildTabViewAt().Selected.ToString();
    //        //Button button = FindViewById<Button>(Resource.Id.myButton);
    //        //button.Text = "Swiped Left";
    //    }

    //    void OnSwipeRight()
    //    {
    //        TabWidget.SetCurrentTab(0);
    //        TabHost.SetCurrentTabByTag("whats_r");
    //        //TabHost.Animation.

    //        //var rintent = new Intent(this, typeof(WeatherForeCastActivity));
    //        //StartActivity(rintent);

    //        // Button button = FindViewById<Button>(Resource.Id.myButton);
    //        //TabWidget.GetChildAt(0).SetBackgroundColor(Android.Graphics.Color.ParseColor("#e18a9e"));
    //    }

    //    public void OnLongPress(MotionEvent e)
    //    {
    //        TabWidget.GetChildAt(1).Selected.ToString();
    //       // TabWidget.GetChildAt(1).SetBackgroundColor(Android.Graphics.Color.ParseColor("#e18a9e"));
    //    }

    //    public bool OnScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
    //    {
    //        return false;
    //    }

    //    public void OnShowPress(MotionEvent e)
    //    {
    //    }

    //    public bool OnSingleTapUp(MotionEvent e)
    //    {
    //        return false;
    //    }

    //    public bool OnTouch(View v, MotionEvent e)
    //    {
    //        return gestureDetector.OnTouchEvent(e);
    //    }

    //    public override bool OnTouchEvent(MotionEvent e)
    //    {
    //        gestureDetector.OnTouchEvent(e);
    //        return false;
    //    }


        // private Android.Support.V4.View.ViewPager mViewPager;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.MenuTabActivity);
            //gestureDetector = new GestureDetector(this);

            var pager = FindViewById<ViewPager>(Resource.Id.pager);
            var tabLayout = FindViewById<Android.Support.Design.Widget.TabLayout>(Resource.Id.sliding_tabs);
            var adapter = new CustomPagerAdapter(this, SupportFragmentManager);
            //var toolbar = FindViewById<Toolbar>(Resource.Id.my_toolbar);

            // Set adapter to view pager
            pager.Adapter = adapter;

            // Setup tablayout with view pager
            tabLayout.SetupWithViewPager(pager);

            // Iterate over all tabs and set the custom view
            for (int i = 0; i < tabLayout.TabCount; i++)
            {
                TabLayout.Tab tab = tabLayout.GetTabAt(i);
                tab.SetCustomView(adapter.GetTabView(i));
            }
            // mViewPager = (Android.Support.V4.View.ViewPager)FindViewById(Resource.Id.viewpager);

            //CreateTab(typeof(MainActivity), "whats_r", "Weather now", Resource.Drawable.TabStyle);
            //CreateTab(typeof(WeatherForeCastActivity), "whats_l", "Tomorrow", Resource.Drawable.TabStyle);



            //TabHost.SetOnClickListener(this);
            //TabHost.SetOnTabChangedListener(this);
            //TabWidget.GetChildAt(0).SetBackgroundColor(Android.Graphics.Color.ParseColor("#8ACAE1"));
            // TabWidget.GetChildAt(1).SetBackgroundColor(Android.Graphics.Color.ParseColor("#8ACAE1"));
            //TabWidget.GetChildAt(0).SetBackgroundResource((Resource.Drawable.ic_tab_my_schedule));

            //TabWidget.GetChildAt(0).SetPadding(100, 100, 100, 100);

            //TabWidget tw = (ImageView)TabHost.FindViewById<ImageView>(Resource.Id.tabImageView);
            // View tabView = tw.GetChildAt(0);
            // tabView.SetBackgroundDrawable(Resources.GetDrawable(Resource.Drawable.ic_tab_my_schedule));
        }

        //private void CreateTab(Type activityType, string tag, string label, int drawableId)
        //{

        //    var intent = new Intent(this, activityType);
        //    intent.AddFlags(ActivityFlags.NewTask);
            
        //    var spec = TabHost.NewTabSpec(tag);
            
        //    // var drawableIcon = Resources.GetDrawable(drawableId);
        //    View tab = LayoutInflater.Inflate(drawableId, null,false);
        //    var textView = tab.FindViewById<TextView>(Resource.Id.tabText);
        //    textView.Text = label;
        //    //string a = Resources.GetString(Resource.Id.textViewkl);
        //    //((TextView)tab.FindViewById<TextView>().SetText(label.ToString());
        //    //TextView text = (TextView)tab.FindViewById<TextView>(Resource.Id.textView);
        //    //text.SetText("bhku");

        //    spec.SetIndicator(tab);
            
        //    //spec.SetIndicator(label);

        //    spec.SetContent(intent);
            
        //    TabHost.AddTab(spec);
                
        //}

        //public void OnTabChanged(string tabs)
        //{
        //    throw new NotImplementedException();
        //}
    }



}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;
using Java.Lang;
using com.Weather.App.Fragments;

namespace com.Weather.App
{
    class CustomPagerAdapter : FragmentPagerAdapter

    {
        const int PAGE_COUNT = 2;
        private string[] tabTitles = { "Tab1", "Tab2" };
        readonly Context context;

        public CustomPagerAdapter(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public CustomPagerAdapter(Context context, Android.Support.V4.App.FragmentManager fm) : base(fm)
        {
            this.context = context;
        }

        public override int Count
        {
            get { return PAGE_COUNT; }
        }

        public override Android.Support.V4.App.Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    return new WeatherToday();
                case 1:
                    return new ForecastWeather();
                default:
                     return null;
            }
            //return PageFragment.newInstance(position + 1);
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            // Generate title based on item position
            return CharSequence.ArrayFromStringArray(tabTitles)[position];
        }

        public View GetTabView(int position)
        {

            var tab = (View)LayoutInflater.From(context).Inflate(Resource.Drawable.TabStyle, null);
            var textView = tab.FindViewById<TextView>(Resource.Id.tabText);
            //    textView.Text = label;
            textView.Text = tabTitles[position];
            return tab;

            //switch (position)
            //{
            //    case 1:
            //        var tab = (View)LayoutInflater.From(context).Inflate(Resource.Drawable.TabStyle, null);
            //        var textView = tab.FindViewById<TextView>(Resource.Id.tabText);
            //       //    textView.Text = label;
            //        textView.Text = tabTitles[position];
            //        return tab;
            //    case 2:
            //        var tab2 = (View)LayoutInflater.From(context).Inflate(Resource.Drawable.TabStyle, null);
            //        var textView2 = tab2.FindViewById<TextView>(Resource.Id.tabText);
            //        //    textView.Text = label;
            //        textView2.Text = tabTitles[position];
            //        return tab2;

            //    default:
            //        return null;
            //}
            // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView
            
        }
    }
}
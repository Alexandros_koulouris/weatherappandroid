package md56c8c690f297e4f4701f14a992fd51ef0;


public class MenuTabActivity
	extends android.support.v7.app.AppCompatActivity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("com.Weather.App.MenuTabActivity, com.Weather.App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MenuTabActivity.class, __md_methods);
	}


	public MenuTabActivity ()
	{
		super ();
		if (getClass () == MenuTabActivity.class)
			mono.android.TypeManager.Activate ("com.Weather.App.MenuTabActivity, com.Weather.App, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
